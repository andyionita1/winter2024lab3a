public class Student {
	
	public String name;
	public int age;
	public String id;
	
	public void study() {
		System.out.println("Currently studying");
	}
	
	public void homework() {
		System.out.println("Currently doing homework");
	}
	
}