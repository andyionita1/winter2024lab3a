public class Application {
	
	public static void main (String[] args) {
		
		Student andy = new Student();
		andy.name = "Andy Ionita";
		andy.age = 27;
		andy.id = "2333068";
		
		Student tin = new Student();
		tin.name = "Tin Tran";
		tin.age = 52;
		tin.id = "2433291";
		
		System.out.println(tin.name + " || Age: " + tin.age + " || ID: " + tin.id);
		
		tin.study();
		tin.homework();
		
		
		Student angie = new Student();
		angie.name = "Angie Fu";
		angie.age = 79;
		angie.id = "2003804";
		
		System.out.println(angie.name + " || Age: " + angie.age + " || ID: " + angie.id);
		
		angie.study();
		angie.homework();
		
		
		Student[] section4 = new Student[3];
		section4[0] = tin;
		section4[1] = angie;
		section4[2] = andy;
		
		System.out.println(section4[0].name);
		System.out.println(section4[1].name);		
		System.out.println(section4[2].name);
	}
}